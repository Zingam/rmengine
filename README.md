RMEngine
========

Description
-----------

Multiplatform OpenGL ES 2.0 based framework

To-Do
-----

1. Initial support for Windows
2. Initial support for Linux
3. Tizen port

Structure
---------

+ Solution
\- RMEngine + 
            \- external (External dependancies)
            \- source (Source files)

