#pragma once
#ifndef APPLICATION_H
#define APPLICATION_H

namespace RMEngine 
{

    class Application
    {
    public:
        Application();
        ~Application();

        // Starts the application
        int start();
    };


}

#endif APPLICATION_H