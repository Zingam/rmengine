#pragma once
#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include "GL/glew.h"

namespace RMEngine 
{

    class Window
    {
    public:
        Window();
        ~Window();

        void CreateWindow();
        void InitializeOpenGL();
    };

}

#endif OPENGLWINDOW_H
